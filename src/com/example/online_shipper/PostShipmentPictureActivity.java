package com.example.online_shipper;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.example.online_shipper.library.FileChooser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class PostShipmentPictureActivity extends Activity {
	
	private static final int FILE_SELECT_CODE = 0;
	Bitmap bm;
	String shipmentId;
	String fname;
	String TARGET_POST = "http://192.168.56.1/tugas_akhir/api/shipment/gallery/";
	
	private void showFileChooser() {
		
		File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "MyApp");
        // Create the storage directory if it does not exist
        if (! imageStorageDir.exists()){
            imageStorageDir.mkdirs();                  
        }
		
		File file = new File(imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");  
        Uri imageUri = Uri.fromFile(file); 
        
		
		final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            cameraIntents.add(intent);
        }
		
		
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT); 
	    intent.setType("image/*"); 
	    intent.addCategory(Intent.CATEGORY_OPENABLE);
	    
	    try {
	    	
	    	Intent chooserIntent = Intent.createChooser(intent,"Image Chooser");
	    	chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
	        startActivityForResult(
	        		chooserIntent,
	                FILE_SELECT_CODE);
	    } catch (android.content.ActivityNotFoundException ex) {
	        // Potentially direct the user to the Market with a Dialog
	        Toast.makeText(this, "Please install a File Manager.", 
	                Toast.LENGTH_SHORT).show();
	    }
	}
	
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shipper_add_picture);
		
		Bundle bundle = getIntent().getExtras();
		shipmentId = bundle.getString("SHIPMENT_DETAIL_ID");
		
		//add pic button
		TextView addPic = (TextView) findViewById(R.id.add_pic);
		addPic.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showFileChooser();
			}
		});
		
		//skip btn, go to thank you page.
		TextView skip = (TextView) findViewById(R.id.skip);
		skip.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent thankyou = new Intent(getApplicationContext(), ThankYouActivity.class);
				thankyou.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        startActivity(thankyou);
		        finish();
			}
			
		});
		
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String resp = "An error occured. Please try again";
	    switch (requestCode) {
	        case FILE_SELECT_CODE:
	        if (resultCode == RESULT_OK) { 
	            Uri uri = data.getData();
	            try{
	            	String path = FileChooser.getPath(this, uri);
	            	File f = new File("" + path);
	            	fname = f.getName();
	            	
	            	
	            	bm = BitmapFactory.decodeFile(path);
	            	
	            	//call uploader
	            	executeMultipartPost();
	            	
	            } catch(Exception e){
	            	Toast.makeText(PostShipmentPictureActivity.this, resp, Toast.LENGTH_LONG).show();
	            }
	        }
	        break;
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	    
	}
	
	public void executeMultipartPost() throws Exception {
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		ProgressDialog pDialog = new ProgressDialog(PostShipmentPictureActivity.this);
        pDialog.setMessage("Uploading..");
        pDialog.setCancelable(false);
        pDialog.show();
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost postRequest = new HttpPost(TARGET_POST);
		MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		reqEntity.addPart("shipmentid", new StringBody(shipmentId));
		
		try{
			
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bm.compress(CompressFormat.JPEG, 75, bos);
			byte[] data = bos.toByteArray();
			ByteArrayBody bab = new ByteArrayBody(data, fname);
			
			reqEntity.addPart("picture", bab);
			
		} catch(Exception e){
			Log.v("Exception in Image", ""+e);
		}
		
		postRequest.setEntity(reqEntity);
		HttpResponse response = httpClient.execute(postRequest);
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
		String sResponse;
	    StringBuilder s = new StringBuilder();
	    while ((sResponse = reader.readLine()) != null) {
	        s = s.append(sResponse);
	    }
	    
	    JSONObject jObj = new JSONObject(s.toString());
	    if (pDialog.isShowing())
            pDialog.dismiss();
	    
	    Toast.makeText(PostShipmentPictureActivity.this, jObj.getString("message"), Toast.LENGTH_LONG).show();
	    
	}
	
}

package com.example.online_shipper;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class RegisterActivity extends Activity {
	
	EditText emailInput,passwordInput,addressInput,cityInput,zipcodeInput;
	Spinner provinceInput;
	Button registerBtn;
	
	
	public final static boolean isValidEmail(CharSequence target) {
	    if (target == null) {
	        return false;
	    } else {
	        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	    }
	}
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		
		//form id linking
		emailInput 		= (EditText) findViewById(R.id.register_email);
		passwordInput 	= (EditText) findViewById(R.id.register_password);
		addressInput 	= (EditText) findViewById(R.id.register_address);
		cityInput 		= (EditText) findViewById(R.id.register_city);
		zipcodeInput 	= (EditText) findViewById(R.id.register_zipcode);
		
		provinceInput	= (Spinner) findViewById(R.id.register_province);
		
		
		registerBtn = (Button) findViewById(R.id.regisnow);
		registerBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				
				
				String email = emailInput.getText().toString();
				String password = passwordInput.getText().toString();
				String address = addressInput.getText().toString();
				String city = cityInput.getText().toString();
				String zipcode = zipcodeInput.getText().toString();
				String province = provinceInput.getSelectedItem().toString();
				
				//validation form
				if(email.length() == 0)
				{
					emailInput.setError("Email address required!");
				}
				if(!isValidEmail(email)){
					emailInput.setError(email + " is not valid email address");
				}
				if(password.length() == 0)
				{
					passwordInput.setError("Password required!");
				}
				if(address.length() == 0)
				{
					addressInput.setError("Password required!");
				}
				if(city.length() == 0)
				{
					cityInput.setError("Password required!");
				}
				if(zipcode.length() == 0)
				{
					zipcodeInput.setError("Password required!");
				}
				
				
				//build parameter
				HashMap<String, String> param = new HashMap<String, String>();
				param.put("email", email);
				param.put("password", password);
				param.put("address", address);
				param.put("city", city);
				param.put("zipcode", zipcode);
				param.put("province", province);
				
//				Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();
				Intent confirm = new Intent(getApplicationContext(), RegisterConfirmActivity.class);
		        
				confirm.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        startActivity(confirm);
		         
		        finish();
					
				
				
				
			}
			
		});
	}
}

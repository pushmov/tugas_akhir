package com.example.online_shipper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RegisterConfirmActivity extends Activity {
	
	Button backButton;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_confirm);
		
		backButton = (Button) findViewById(R.id.backlogin);
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent loginScreen = new Intent(getApplicationContext(), LoginScreenActivity.class);
		        
				loginScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        startActivity(loginScreen);
		        finish();
				
			}
			
		});
	}
}

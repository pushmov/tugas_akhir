package com.example.online_shipper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ThankYouActivity extends Activity {
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shipment_thank_you);
		
		TextView backToLogin = (TextView) findViewById(R.id.back_to_login);
		backToLogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent login = new Intent(getApplicationContext(), LoginScreenActivity.class);
				login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        startActivity(login);
		        finish();
				
			}
			
		});
	}
	
}

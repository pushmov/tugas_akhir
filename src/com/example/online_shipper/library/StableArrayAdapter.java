package com.example.online_shipper.library;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;

public class StableArrayAdapter extends ArrayAdapter<String> {
	
	HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
	
	public StableArrayAdapter(Context context, int textViewResourceId, List<String> objects) {
		super(context, textViewResourceId);
		
		for(int i=0; i<objects.size(); i++){
			mIdMap.put(objects.get(i), i);
		}
		
	}
	
	public long getItemId(int position){
		String item = getItem(position);
		return mIdMap.get(item);
	}
	
	public boolean hasStableId(){
		return true;
	}

}

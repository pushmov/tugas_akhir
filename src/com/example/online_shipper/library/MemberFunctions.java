package com.example.online_shipper.library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;

public class MemberFunctions {
	
	private JSONParser jsonParser;
    
    /** 
     * static URL for localhost mode
     */
	
    private static String login_URL = "http://192.168.56.1/tugas_akhir/api/login/";
    private static String register_URL = "http://192.168.56.1/tugas_akhir/api/user/register/";
    
    private static String login_tag = "login";
    private static String register_tag = "register";
    
    /** constructor */
    public MemberFunctions(){
    	jsonParser = new JSONParser();
    }
    
    /**
     * function : make login request
     * @param email_or_username
     * @param password
     * @return
     */
    public JSONObject loginUser(String email_or_username, String password){
    	List<NameValuePair> params = new ArrayList<NameValuePair>();
    	params.add(new BasicNameValuePair("login",login_tag));
    	params.add(new BasicNameValuePair("email",email_or_username));
    	params.add(new BasicNameValuePair("password",password));
    	JSONObject json = jsonParser.getJSONFromUrl(login_URL, params);
    	return json;
    }
    
    /**
     * function to check if the user is logged in or not
     * @param context
     * @return
     */
    public boolean isUserLoggedIn(Context context){
    	DatabaseHandler db = new DatabaseHandler(context);
    	int count = db.getRowCount();
        if(count > 0){
            // user logged in
            return true;
        }
        return false;
    }
    
    /**
     * register user function
     * @param hashMapParams
     * @return
     */
    public JSONObject registerUser(HashMap<String, String> hashMapParams){
    	List<NameValuePair> params = new ArrayList<NameValuePair>();
    	params.add(new BasicNameValuePair("register",register_tag));
    	params.add(new BasicNameValuePair("email",hashMapParams.get("email")));
    	params.add(new BasicNameValuePair("password",hashMapParams.get("password")));
    	params.add(new BasicNameValuePair("city",hashMapParams.get("city")));
    	params.add(new BasicNameValuePair("province",hashMapParams.get("province")));
    	params.add(new BasicNameValuePair("zipcode",hashMapParams.get("zipcode")));
    	params.add(new BasicNameValuePair("address",hashMapParams.get("address")));
    	JSONObject json = jsonParser.getJSONFromUrl(register_URL, params);
    	return json;
    }
    
    /**
     * function to logout user
     * @param context
     * @return
     */
    public boolean logoutUser(Context context){
    	DatabaseHandler db = new DatabaseHandler(context);
        db.resetTables();
        return true;
    }
	
}

package com.example.online_shipper.library;

import java.util.HashMap;

import com.example.online_shipper.LoginScreenActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Session {
	
	SharedPreferences pref;
	Editor editor;
	
	Context _context;
	
	int PRIVATE_MODE = 0;
	private static final String PREF_NAME = "MySession";
	private static final String IS_LOGIN = "IsLoggedIn";
	private static final String UID = "memberid";
	private static final String EMAIL = "email";
	private static final String USERNAME = "username";
	private static final String FNAME = "fname";
	private static final String MNAME = "mname";
	private static final String LNAME = "lname";
	private static final String ACTIVATION_CODE = "activation_code";
	
	/**
	 * Constructor
	 * @param context
	 */
	public Session(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	
	/**
	 * write the session to shared preferences
	 * @param uid
	 * @param email
	 * @param fname
	 * @param lname
	 */
	public void write(String uid, String email, String username, String fname, String mname, String lname, String acode){
		editor.putBoolean(IS_LOGIN, true);
		editor.putString(UID, uid);
		editor.putString(EMAIL, email);
		editor.putString(USERNAME, username);
		editor.putString(FNAME, fname);
		editor.putString(MNAME, mname);
		editor.putString(LNAME, lname);
		editor.putString(ACTIVATION_CODE, acode);
		editor.commit();
	}
	
	/**
	 * read the session as hashmap
	 * @return
	 */
	public HashMap<String, String> read(){
		HashMap<String, String> storage = new HashMap<String, String>();
		storage.put(UID, pref.getString(UID, null));
		storage.put(EMAIL, pref.getString(EMAIL, null));
		storage.put(USERNAME, pref.getString(USERNAME, null));
		storage.put(FNAME, pref.getString(FNAME, null));
		storage.put(MNAME, pref.getString(MNAME, null));
		storage.put(LNAME, pref.getString(LNAME, null));
		storage.put(ACTIVATION_CODE, pref.getString(ACTIVATION_CODE, null));
		return storage;
	}
	
	/**
	 * clear the session , redirect to the login area
	 */
	public void clear_all(){
		editor.putBoolean(IS_LOGIN, false);
		editor.remove(UID);
		editor.remove(EMAIL);
		editor.remove(USERNAME);
		editor.remove(FNAME);
		editor.remove(MNAME);
		editor.remove(LNAME);
		editor.remove(ACTIVATION_CODE);
		editor.clear();
		editor.commit();
		
		Intent i = new Intent(_context, LoginScreenActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		_context.startActivity(i);
	}
	
	/**
	 * check login status
	 * @return
	 */
	public boolean isLoggedIn(){
		return pref.getBoolean(IS_LOGIN, false);
	}
	
}

package com.example.online_shipper.library;

public class ReverseGeocoding {
	
	//field that received from reverse geocoding from gmaps
	private String street_number;
	private String route;
	private String locality;
	private String administrative_area_level_3;
	private String administrative_area_level_2;
	private String administrative_area_level_1;
	private String country;
	private String postal_code;
	
	public ReverseGeocoding(){}
	
	public ReverseGeocoding(String street_number,String route, String locality, String administrative_area_level_3, 
			String administrative_area_level_2, String administrative_area_level_1, String country, String postal_code){
		this.street_number = street_number;
		this.setRoute(route);
		this.setLocality(locality);
		this.setAdministrativeAreaLevel3(administrative_area_level_3);
		this.setAdministrativeAreaLevel2(administrative_area_level_2);
		this.setAdministrativeAreaLevel1(administrative_area_level_1);
		this.setCountry(country);
		this.setPostalCode(postal_code);
	}
	
	public String getStreetNumber() {
		return street_number;
	}
	public void setStreetNumber(String street_number) {
		this.street_number = street_number;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getAdministrativeAreaLevel3() {
		return administrative_area_level_3;
	}

	public void setAdministrativeAreaLevel3(String administrative_area_level_3) {
		this.administrative_area_level_3 = administrative_area_level_3;
	}

	public String getAdministrativeAreaLevel2() {
		return administrative_area_level_2;
	}

	public void setAdministrativeAreaLevel2(String administrative_area_level_2) {
		this.administrative_area_level_2 = administrative_area_level_2;
	}

	public String getAdministrativeAreaLevel1() {
		return administrative_area_level_1;
	}

	public void setAdministrativeAreaLevel1(String administrative_area_level_1) {
		this.administrative_area_level_1 = administrative_area_level_1;
	}

	public String getPostalCode() {
		return postal_code;
	}

	public void setPostalCode(String postal_code) {
		this.postal_code = postal_code;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
}

package com.example.online_shipper;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class RegisterTypeActivity extends Activity {
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_type);
		
		TextView registerAsDriver = (TextView) findViewById(R.id.reg_as_driver);
		TextView registerAsShipper = (TextView) findViewById(R.id.reg_as_shipper);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		registerAsDriver.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent register = new Intent(getApplicationContext(), RegisterActivity.class);
		        
				register.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        startActivity(register);
		         
		        finish();
			}
			
		});
		
		
		
		registerAsShipper.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {				
				Intent postShipment = new Intent(getApplicationContext(), PostShipmentActivity.class);
		        
		        // Close all views before launching Dashboard
				postShipment.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        startActivity(postShipment);
				
			}
			
		});
	}
	
	
	
	public boolean onOptionsItemSelected(MenuItem item){
		
		Intent myIntent = new Intent(getApplicationContext(), LoginScreenActivity.class);
	    startActivityForResult(myIntent, 0);
	    return true;
	    
	}
}

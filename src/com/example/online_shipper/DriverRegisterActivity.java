package com.example.online_shipper;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.online_shipper.library.HTTPRequest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DriverRegisterActivity extends Activity {
	int status_newsletter = 0;
	int status_notification = 0;
	
	ProgressDialog pDialog;
	
	final String TARGET_POST = "http://192.168.56.1/tugas_akhir/api/driver/register/";
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.driver_register);
		
		EditText driverEmail = (EditText) findViewById(R.id.driver_email);
		EditText driverPassword = (EditText) findViewById(R.id.driver_password);
		EditText driverPhone = (EditText) findViewById(R.id.driver_phone);
		
		CheckBox subscribeToNewsLetter = (CheckBox) findViewById(R.id.subscribe_to_newsletter);
		CheckBox subscribeToNotification = (CheckBox) findViewById(R.id.subscribe_to_notification);
		
		
		if(subscribeToNewsLetter.isChecked()){
			status_newsletter = 1;
		}
		
		if(subscribeToNotification.isChecked()){
			status_notification = 1;
		}
		
		//validation
		final String strEmail = driverEmail.getText().toString();
		if(strEmail.length() == 0){
			driverEmail.setError("email required");
		}
		
		final String strPassword = driverPassword.getText().toString();
		if(strPassword.length() == 0){
			driverPassword.setError("password required");
		}
		
		final String strPhone = driverPhone.getText().toString();
		if(strPhone.length() == 0){
			driverPhone.setError("phone number required");
		}
		
		CheckBox tos = (CheckBox) findViewById(R.id.tos);
		if(!tos.isChecked()){
			tos.setError("You must indicate that you have read and understood the Transporter Agreement and Terms of Use");
			Toast.makeText(DriverRegisterActivity.this, "You must indicate that you have read and understood the Transporter Agreement and Terms of Use", Toast.LENGTH_LONG).show();
		}
		
		TextView registerBtn = (TextView) findViewById(R.id.driver_register_btn);
		registerBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("unchecked")
			@Override
			public void onClick(View arg0) {
				
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("driver_email", strEmail));
				params.add(new BasicNameValuePair("driver_password", strPassword));
				params.add(new BasicNameValuePair("driver_phone", strPhone));
				params.add(new BasicNameValuePair("newsletter", String.valueOf(status_newsletter)));
				params.add(new BasicNameValuePair("notification", String.valueOf(status_notification)));
				
				new NewDriver().execute(params);
			}
		});
	}
	
	private class NewDriver extends AsyncTask<List<NameValuePair>,Void,Void>{
		
		protected void onPreExecute() {
	        super.onPreExecute();
	        pDialog = new ProgressDialog(DriverRegisterActivity.this);
	        pDialog.setMessage("Processing..");
	        pDialog.setCancelable(false);
	        pDialog.show();
	    }
		
		@Override
		protected Void doInBackground(List<NameValuePair>... arg0) {
			
			final List<NameValuePair> params = arg0[0];
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
			
			DriverRegisterActivity.this.runOnUiThread(new Runnable(){

				@Override
				public void run() {
					HTTPRequest httpRequest = new HTTPRequest(TARGET_POST);
					String response = httpRequest.makeRequest(HTTPRequest.POST, params);
					try{
						
						JSONObject jObj = new JSONObject(response);
						String status = jObj.getString("status");
						String message = jObj.getString("message");
						Toast.makeText(DriverRegisterActivity.this, message, Toast.LENGTH_LONG).show();
						if(status.equals("OK")){
							//redirect here
						}
						
					}catch(JSONException j){
						Toast.makeText(DriverRegisterActivity.this, "An error occured. Please try again", Toast.LENGTH_LONG).show();
					}
				}
				
			});
			return null;
		}
		
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
	        if (pDialog.isShowing())
	            pDialog.dismiss();
	    }
		
	}
	
}

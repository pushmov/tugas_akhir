package com.example.online_shipper.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.online_shipper.R;
import com.example.online_shipper.ShipmentDetailActivity;
import com.example.online_shipper.adapter.FindShipmentsList;
import com.example.online_shipper.adapter.FindShipmentsListAdapter;
import com.example.online_shipper.functions.FindShipmentFunctions;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FindShipmentsFragment extends Fragment {
	
	private static String SHIPMENT_ID = "shipment_id";
	private static String SHIPMENT_NAME = "name";
	private static String SHIPMENT_TYPE = "shipment_type_name";
	private static String DATE_POSTED = "date_added";
	private static String ICON = "icon_name";
	
	ArrayList<FindShipmentsList> results;
	ListView listview;
	FindShipmentsListAdapter adapter;
	
	int current_page = 1;
	ProgressDialog pDialog;
	int counter = 0;
	
	public String URL = "http://192.168.56.1/tugas_akhir/api/shipment/all/";
	
	public FindShipmentsFragment(){}
	
	public ArrayList<FindShipmentsList> parseResponse(JSONArray json){
		ArrayList<FindShipmentsList> results = new ArrayList<FindShipmentsList>();
		counter += json.length();
		for (int i = 0; i < json.length(); i++) {
			
			try{
				
				JSONObject row = json.getJSONObject(i);
				String shipment_category = row.getString(SHIPMENT_TYPE).toString();
				String shipment_name = row.getString(SHIPMENT_NAME).toString();
				String date_posted = row.getString(DATE_POSTED).toString();
				String icon = row.getString(ICON).toString();
				String shipment_id = row.getString(SHIPMENT_ID).toString();
				
				FindShipmentsList sr = new FindShipmentsList();
				
				sr.setShipmentCategory(shipment_category);
				sr.setShipmentName(shipment_name);
				sr.setDatePosted(date_posted);
				sr.setIcon(icon);
				sr.setShipmentId(shipment_id);
				results.add(sr);
				
			} catch (JSONException j){
				Toast.makeText(getActivity(), "JSON Exception", Toast.LENGTH_LONG).show();
			}
		
        }
		
		return results;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View rootView = inflater.inflate(R.layout.find_shipments_fragment, container, false);
		
		listview = (ListView) rootView.findViewById(R.id.listview);
		
		//add load more button
		
		TextView btnLoadMoreNew = new TextView(getActivity());
		btnLoadMoreNew.setText("load more");
		btnLoadMoreNew.setBackgroundResource(R.drawable.blue_grad);
		btnLoadMoreNew.setGravity(Gravity.CENTER);
		btnLoadMoreNew.setTextColor(Color.WHITE);
		btnLoadMoreNew.setPadding(18, 18, 18, 18);
		
		listview.addFooterView(btnLoadMoreNew);
		
		FindShipmentFunctions findShipmentFunc = new FindShipmentFunctions();
		JSONArray jsonArray = findShipmentFunc.allshipments(URL);
		
		//parse json response
		results = parseResponse(jsonArray);
		adapter = new FindShipmentsListAdapter(getActivity(),results);
		listview.setAdapter(adapter);
		
		btnLoadMoreNew.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				new loadMoreListView().execute();
				
			}
		});
		
		
		//go to detail shipment page
		listview.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				
				TextView textViewShipmentId = (TextView) view.findViewById(R.id.shipment_id);
				String shipment_id = textViewShipmentId.getText().toString(); 
				
				Intent intent = new Intent(getActivity(), ShipmentDetailActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("SHIPMENT_DETAIL_ID", shipment_id);
				intent.putExtras(bundle);
				startActivity(intent);
				
			}
			
		});
		
		return rootView;
	}
	
	
	private class loadMoreListView extends AsyncTask<Void, Void, Void> {
		ArrayList<FindShipmentsList> asyncresults;
		@Override
        protected void onPreExecute() {
            // Showing progress dialog before sending http request
            pDialog = new ProgressDialog(
                    getActivity());
            pDialog.setMessage("Please wait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }
		
		@Override
		protected Void doInBackground(Void... arg0) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					current_page += 1;
					FindShipmentFunctions findShipmentFunctions = new FindShipmentFunctions();
					JSONArray response = findShipmentFunctions.allshipments(URL+current_page); 
					asyncresults = parseResponse(response);
					
					
					
					// get listview current position - used to maintain scroll position
					int currentPosition = listview.getFirstVisiblePosition();
					adapter = new FindShipmentsListAdapter(getActivity(),asyncresults);
					adapter.notifyDataSetChanged();
					listview.setAdapter(adapter);
					
					// Setting new scroll position
					listview.setSelectionFromTop(currentPosition + 1, 0);
					
					
				}
				
			});
			return null;
		}
		
		protected void onPostExecute(Void unused) {
            // closing progress dialog
            pDialog.dismiss();
        }
		
	}
}

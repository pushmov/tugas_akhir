package com.example.online_shipper.functions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.online_shipper.library.HTTPRequest;

import android.util.Log;

public class FindShipmentFunctions {
	
    static JSONArray jArray = null;
    static JSONObject jObj = null;

	public String url;
    
    public FindShipmentFunctions(){}
    
    public JSONObject shipmentDetail(String url){
    	
    	HTTPRequest httpRequest = new HTTPRequest(url);
    	String response = httpRequest.makeRequest(HTTPRequest.GET, null);
    	try {
            jObj = new JSONObject(response);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
    	
    	return jObj;
    }
    
    public int totalShipments(String url){
    	int totalShipments = 0;
    	
    	HTTPRequest httpRequest = new HTTPRequest(url);
    	String response = httpRequest.makeRequest(HTTPRequest.GET, null);
 
        // try parse the string to a JSON object
        try {
            JSONObject jObj = new JSONObject(response);
            String total = jObj.getString("total");
            totalShipments = Integer.parseInt(total);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        
        return totalShipments;
    }
    
    public JSONArray allshipments(String url){
    	
    	HTTPRequest httpRequest = new HTTPRequest(url);
    	String json = httpRequest.makeRequest(HTTPRequest.GET, null);
        // try parse the string to a JSON object
        try {
            jArray = new JSONArray(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
 
        // return JSON String
        return jArray;
    }
    
    
}

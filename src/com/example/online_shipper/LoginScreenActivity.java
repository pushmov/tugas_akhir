package com.example.online_shipper;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.online_shipper.library.ConnectionDetector;
import com.example.online_shipper.library.MemberFunctions;
import com.example.online_shipper.library.Session;

public class LoginScreenActivity extends Activity {
	
	Button btnLogin;
	EditText input_email_or_username;
	EditText input_password;
	TextView loginErrorMsg;
	TextView registerBtn;
	ProgressDialog pDialog;
	
	// JSON Response node names
	private static String KEY_CONTROLLER = "login";
	private static String KEY_MESSAGE = "message";
	
	private static String KEY_SUCCESS = "success";
	
	
	private static String PROP_UID = "member_id";
	private static String PROP_EMAIL = "email";
	private static String PROP_USERNAME = "username";
	private static String PROP_FNAME = "first_name";
	private static String PROP_MNAME = "middle_name";
	private static String PROP_LNAME = "last_name";
	private static String PROP_ACTIVATION_CODE = "activation_code";
	
	private void startDashboardActivity(){
		Intent dashboard = new Intent(getApplicationContext(), DashboardActivity.class);
        
        // Close all views before launching Dashboard
        dashboard.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(dashboard);
         
        // Close Login Screen
        finish();
	}
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		
		
		//call the session if user existing logged in
//		Session session = new Session(getApplicationContext());
//		if(session.isLoggedIn()){
//			startDashboardActivity();
//		}
		
		// Importing all assets like buttons, text fields
		input_email_or_username = (EditText) findViewById(R.id.user_email_input);		
		input_password = (EditText) findViewById(R.id.password);
		
		TextView newBtnLogin = (TextView) findViewById(R.id.sign_in);
		
		// Login button Click Event
		newBtnLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				
				final String email_or_username = input_email_or_username.getText().toString();
				final String password = input_password.getText().toString();
				
				if(email_or_username.length() == 0)
				{
					input_email_or_username.setError("Login name or email required!");
				}
				if(password.length() == 0){
					input_password.setError("Password required!");
				}
				
		    	
				ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
				if(cd.isConnectingToInternet()){
					
					StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
					StrictMode.setThreadPolicy(policy);
					
					final ProgressDialog ringProgressDialog = ProgressDialog.show(LoginScreenActivity.this, "Please wait ...", "Signing In ...", true);
					ringProgressDialog.setCancelable(true);
					new Thread(new Runnable() {
	
						@Override
						public void run() {
							 try {
								 MemberFunctions memberFunctions = new MemberFunctions();
							    	JSONObject json = memberFunctions.loginUser(email_or_username, password);
									
									try {
										
						                if (json.getString(KEY_SUCCESS) != null) {   
						                    String res = json.getString(KEY_SUCCESS);
						                    
						                    if(res.equals("success")){
						                        // user successfully logged in
						                        JSONObject json_user = json.getJSONObject(KEY_CONTROLLER);
						                        Session session = new Session(getApplicationContext());
						                        session.write(json_user.getString(PROP_UID), 
						                        		json_user.getString(PROP_EMAIL),
						                        		json_user.getString(PROP_USERNAME),
						                        		json_user.getString(PROP_FNAME), 
						                        		json_user.getString(PROP_MNAME),
						                        		json_user.getString(PROP_LNAME),
						                        		json_user.getString(PROP_ACTIVATION_CODE)
						                        );
						                        // Launch Dashboard Screen
						                        ringProgressDialog.dismiss();
						                        startDashboardActivity();
						                        
						                    } else {
						                    	String error_message = json.getString(KEY_MESSAGE);
						                        // Error in login
						                    	ringProgressDialog.dismiss();
						                    	Toast.makeText(getApplicationContext(), error_message, Toast.LENGTH_LONG).show();
						                    }
						                }
						                
						                
						            } catch (JSONException e) {
						                e.printStackTrace();
						            }
							 } catch (Exception e) {
								 ringProgressDialog.dismiss();
								 Toast.makeText(LoginScreenActivity.this, "Exception Error", Toast.LENGTH_LONG).show();
							 }
							 ringProgressDialog.dismiss();
						}
						
					}).start();
					
				} else {
					Toast.makeText(LoginScreenActivity.this, "Please enable internet connection", Toast.LENGTH_LONG).show();
				}
				
			}
		});
		
		/**
		 * Register btn listener
		 * */
		registerBtn = (TextView) findViewById(R.id.register);
		registerBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent register = new Intent(getApplicationContext(), RegisterTypeActivity.class);
		        
				register.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        startActivity(register);
		        finish();
				
//				GPSTracker gpsTracker = new GPSTracker(getApplicationContext());
//				if(gpsTracker.canGetLocation()){
//					double latitude = gpsTracker.getLatitude();
//					double longitude = gpsTracker.getLongitude();
//					Toast.makeText(getApplicationContext(), "Latitude : "+latitude+" & Longitude : "+longitude, Toast.LENGTH_LONG).show();
//				}else{
//					gpsTracker.showSettingsAlert();
//				}
			}
			
		});
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}

package com.example.online_shipper.adapter;

public class FindShipmentsList {
	private String shipment_category;
	private String shipment_name;
	private String date_posted;
	private String icon;
	private String shipment_id;
	
	public FindShipmentsList(){}

	public String getShipmentCategory() {
		return shipment_category;
	}

	public void setShipmentCategory(String shipment_category) {
		this.shipment_category = shipment_category;
	}

	public String getShipmentName() {
		return shipment_name;
	}

	public void setShipmentName(String shipment_name) {
		this.shipment_name = shipment_name;
	}

	public String getDatePosted() {
		return date_posted;
	}

	public void setDatePosted(String date_posted) {
		this.date_posted = date_posted;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getShipmentId() {
		return shipment_id;
	}

	public void setShipmentId(String shipment_id) {
		this.shipment_id = shipment_id;
	}
	
	
	
	
}

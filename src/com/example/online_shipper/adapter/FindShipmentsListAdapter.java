package com.example.online_shipper.adapter;

import java.util.ArrayList;
import com.example.online_shipper.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FindShipmentsListAdapter extends BaseAdapter {
	
	private static ArrayList<FindShipmentsList> searchArrayList;
	private LayoutInflater mInflater;
	
	public FindShipmentsListAdapter(Context context, ArrayList<FindShipmentsList> results) {
		searchArrayList = results;
		mInflater = LayoutInflater.from(context);
    }
	
	@Override
	public int getCount() {
		return searchArrayList.size();
	}
	
	

	@Override
    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.find_shipment_row, null);
			holder = new ViewHolder();
			
			holder.shipmentType = (TextView) convertView.findViewById(R.id.shipment_category);
			holder.shipmentName = (TextView) convertView.findViewById(R.id.shipment_name);
			holder.datePosted = (TextView) convertView.findViewById(R.id.date_posted);
			holder.icon = (ImageView) convertView.findViewById(R.id.icon);
			holder.shipmentId = (TextView) convertView.findViewById(R.id.shipment_id);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.shipmentType.setText(searchArrayList.get(position).getShipmentCategory());
		holder.shipmentName.setText(searchArrayList.get(position).getShipmentName());
		holder.datePosted.setText(searchArrayList.get(position).getDatePosted());
		holder.shipmentId.setText(searchArrayList.get(position).getShipmentId());
		
		return convertView;
	}
	
	static class ViewHolder{
		TextView shipmentType;
		TextView shipmentName;
		TextView datePosted;
		ImageView icon;
		TextView shipmentId;
	}

}

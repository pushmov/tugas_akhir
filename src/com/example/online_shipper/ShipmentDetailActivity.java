package com.example.online_shipper;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.online_shipper.functions.FindShipmentFunctions;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ShipmentDetailActivity extends Activity {
	
	public String URL = "http://192.168.56.1/tugas_akhir/api/shipment/detail/";
	private String TAG_SUCCESS = "success";
	
	public double pickLat;
	public double pickLon;
	
	public double deliverLat;
	public double deliverLon;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shipment_detail);
		
		Bundle bundle = getIntent().getExtras();
		String shipmentId = bundle.getString("SHIPMENT_DETAIL_ID");
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
	    ImagePagerAdapter adapter = new ImagePagerAdapter();
	    viewPager.setAdapter(adapter);
		
		/** create http get request */
		FindShipmentFunctions findShipmentFunctions = new FindShipmentFunctions();
		JSONObject jObj = findShipmentFunctions.shipmentDetail(URL+shipmentId);
		try{
			
			String success = jObj.getString(TAG_SUCCESS);
			if(success.equals(TAG_SUCCESS)){
				
				JSONObject shipmentObjDetail = jObj.getJSONObject("result");
				
				//pick, drop geo location
				pickLat = Double.parseDouble(shipmentObjDetail.getString("pickup_latitude"));
				pickLon = Double.parseDouble(shipmentObjDetail.getString("pickup_longitude"));
				
				deliverLat = Double.parseDouble(shipmentObjDetail.getString("deliver_latitude"));
				deliverLon = Double.parseDouble(shipmentObjDetail.getString("deliver_longitude"));
				
				
				//inject all field
				TextView category = (TextView) findViewById(R.id.shipment_category);
				category.setText(shipmentObjDetail.getString("shipment_category"));
				
				TextView name = (TextView) findViewById(R.id.shipment_detail_name);
				name.setText(shipmentObjDetail.getString("name"));
				
				//Pickup From : Bandung, 40266
				TextView pickUpFrom = (TextView) findViewById(R.id.shipment_detail_pickup);
				String pickupText = "Pickup From : "+shipmentObjDetail.getString("pickup_city")+", "+shipmentObjDetail.getString("pickup_zip_code");
				pickUpFrom.setText(pickupText);
				
				//Deliver To : Tegal, 52192
				TextView deliverTo = (TextView) findViewById(R.id.shipment_detail_deliver);
				String deliverText = "Deliver To : "+shipmentObjDetail.getString("deliver_city")+", "+shipmentObjDetail.getString("deliver_zip_code");
				deliverTo.setText(deliverText);
				
				//date expired = date deliver
				TextView dateExpired = (TextView) findViewById(R.id.shipment_date_expired);
				String dateExpiredText = "Exp : "+shipmentObjDetail.getString("expired_date");
				dateExpired.setText(dateExpiredText);
				
				TextView shipmentDetail = (TextView) findViewById(R.id.shipment_detail);
				shipmentDetail.setText(shipmentObjDetail.getString("detail"));
				
				//address pickup
				TextView pickupAddress = (TextView) findViewById(R.id.pickup_address);
				String pickupAddrTxt = shipmentObjDetail.getString("pickup_address");
				pickupAddress.setText(pickupAddrTxt);
				
				TextView pickupCity = (TextView) findViewById(R.id.pickup_city);
				String pickupCityTxt = shipmentObjDetail.getString("pickup_city") +", "+shipmentObjDetail.getString("pickup_province_name");
				pickupCity.setText(pickupCityTxt);
				
				TextView pickupPostalCode = (TextView) findViewById(R.id.pickup_postal_code);
				String postalCodeTxt = shipmentObjDetail.getString("pickup_zip_code")+", "+shipmentObjDetail.getString("country");
				pickupPostalCode.setText(postalCodeTxt);
				
				//address deliver
				TextView deliverAddr = (TextView) findViewById(R.id.deliver_address);
				String deliverAddrTxt = shipmentObjDetail.getString("deliver_address");
				deliverAddr.setText(deliverAddrTxt);
				
				TextView deliverCity = (TextView) findViewById(R.id.deliver_city);
				String deliverCityTxt = shipmentObjDetail.getString("pickup_city") +", "+shipmentObjDetail.getString("pickup_province_name");
				deliverCity.setText(deliverCityTxt);
				
				TextView deliverPostalCode = (TextView) findViewById(R.id.deliver_postal_code);
				String deliverCodeTxt = shipmentObjDetail.getString("pickup_zip_code")+", "+shipmentObjDetail.getString("country");
				deliverPostalCode.setText(deliverCodeTxt);
			}
			
		} catch(JSONException je){
			Toast.makeText(getApplicationContext(), "JSON Exception", Toast.LENGTH_LONG).show();
		}
		
//		TextView test = (TextView) findViewById(R.id.shipment_detail_id);
//		test.setText("SHIPMENTID : "+shipmentId);
	    TextView showPickMap = (TextView) findViewById(R.id.launch_pick_map);
	    showPickMap.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "textview clicked", Toast.LENGTH_LONG).show();
				
				String label = "ABC Label";
				String uriBegin = "geo:" + pickLat + "," + pickLon;
				String query = pickLat + "," + pickLon + "(" + label + ")";
				String encodedQuery = Uri.encode(query);
				String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
				Uri uri = Uri.parse(uriString);
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
				startActivity(intent);
				
			}
		});
	    
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		
		switch (item.getItemId()) {
		case R.id.gmap_icon:
			Toast.makeText(getApplicationContext(), "gmap icon clicked", Toast.LENGTH_LONG).show();
			return true;
		default :
			Intent myIntent = new Intent(getApplicationContext(), DashboardActivity.class);
		    startActivityForResult(myIntent, 0);
		    return true;
		}
	    
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.shipment_detail, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	private class ImagePagerAdapter extends PagerAdapter {
	    private int[] mImages = new int[] {
	        R.drawable.chiang_mai,
	        R.drawable.himeji,
	        R.drawable.petronas_twin_tower,
	        R.drawable.ulm
	    };

	    @Override
	    public int getCount() {
	      return mImages.length;
	    }

	    @Override
	    public boolean isViewFromObject(View view, Object object) {
	      return view == ((ImageView) object);
	    }

	    @Override
	    public Object instantiateItem(ViewGroup container, int position) {
	      Context context = ShipmentDetailActivity.this;
	      ImageView imageView = new ImageView(context);
	      int padding = context.getResources().getDimensionPixelSize(
	          R.dimen.padding_medium);
	      imageView.setPadding(padding, padding, padding, padding);
	      imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
	      imageView.setImageResource(mImages[position]);
	      ((ViewPager) container).addView(imageView, 0);
	      return imageView;
	    }

	    @Override
	    public void destroyItem(ViewGroup container, int position, Object object) {
	      ((ViewPager) container).removeView((ImageView) object);
	    }
	  }
	
	public void launchPickupLocation(View v){
		
		String label = "ABC Label";
		String uriBegin = "geo:" + pickLat + "," + pickLon;
		String query = pickLat + "," + pickLon + "(" + label + ")";
		String encodedQuery = Uri.encode(query);
		String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
		Uri uri = Uri.parse(uriString);
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
		startActivity(intent);
	}
	
}

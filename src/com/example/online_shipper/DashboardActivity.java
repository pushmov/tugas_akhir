package com.example.online_shipper;

import java.util.ArrayList;

import com.example.online_shipper.adapter.NavDrawerListAdapter;
import com.example.online_shipper.fragment.DashboardFragment;
import com.example.online_shipper.fragment.FindShipmentsFragment;
import com.example.online_shipper.library.Session;
import com.example.online_shipper.model.NavDrawerItem;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;



public class DashboardActivity extends Activity {
	
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	
	//nav drawer title
	private CharSequence mDrawerTitle;
	
	//used to store app title
	private CharSequence mTitle;
	
	//slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	
	TextView logout;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
		
		mTitle = mDrawerTitle = getTitle();
		
		//load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.left_drawer_item);
		
		//nav drawer icons from resources
		navMenuIcons = getResources().obtainTypedArray(R.array.left_drawer_icon);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
		
		navDrawerItems = new ArrayList<NavDrawerItem>();
		
		//adding nav drawer items to array
		
		//dashboard
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
		//find shipment
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
		//favorite shipment
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
		//my bids
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1), true, "22"));
		//message
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
		//routes
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1), true, "50+"));
		//feedback
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));
		//logout
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
		
		//recycle typed array
		navMenuIcons.recycle();
		
		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
		
		//setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems);
		mDrawerList.setAdapter(adapter);
		
		//enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
		){
			
			public void onDrawerClosed(View view){
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}
			
			public void onDrawerOpened(View drawerView){
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
			
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		
		if(savedInstanceState == null){
			// on first time display view for first nav item
			displayView(0);
		}
		
	}
	
	private class SlideMenuClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		//toggle nav drawer on selecting action bar app icon/title
		if(mDrawerToggle.onOptionsItemSelected(item)){
			return true;
		}
		
		//Handle actions bar actions click
		switch(item.getItemId()){
			case R.id.action_settings:
				return true;
			default :
				return super.onOptionsItemSelected(item);
		}
	}
	
	/***
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu){
		//if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}
	
	/**
	 * Displaying fragment view for selected nav drawer list item
	 * */
	public void displayView(int position){
		//update the main content by replacing fragments
		Fragment fragment = null;
		switch(position){
			case 0:
				fragment = new DashboardFragment();
				break;
			case 1:
				fragment = new FindShipmentsFragment();
				break;
			case 2:
				fragment = new DashboardFragment();
				break;
			case 3:
				fragment = new DashboardFragment();
				break;
			case 4:
				fragment = new DashboardFragment();
				break;
			case 5:
				fragment = new DashboardFragment();
				break;
			case 6:
				fragment = new DashboardFragment();
				break;
			case 7:
				logoutOperation();
				break;
			default :
				break;
		}
		
		if(fragment != null){
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
			
			//update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			Log.e("MainActivity", "Error in creating fragment");
		}
	}
	
	public void logoutOperation(){
		Session session = new Session(getApplicationContext());
		session.clear_all();
		Intent login = new Intent(getApplicationContext(), LoginScreenActivity.class);
		login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);
         
        // Close Dashboard Screen
        finish();
	}
	
	@Override
	public void setTitle(CharSequence title){
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}
	
	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()..
	 */
	@Override
	protected void onPostCreate(Bundle savedInstanceState){
		super.onPostCreate(savedInstanceState);
		//sync the toggle state after onRestoreInstanceState has occurred
		mDrawerToggle.syncState();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig){
		super.onConfigurationChanged(newConfig);
		//Pass any configuration change to the drawer toggle
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	
}

package com.example.online_shipper;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.online_shipper.library.GPSTracker;
import com.example.online_shipper.library.HTTPRequest;
import com.example.online_shipper.library.Category;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PostShipmentActivity extends Activity {
	
	ProgressDialog pDialog;
	ArrayList<Category> categoriesList;
	Spinner spinnerCategory, spinnerListingDuration;
	EditText checkEmail;
	HashMap<String,String> geoCoding;
	
	boolean IS_USING_CURRENT_POSITION = false;
	
	private static String REVERSE_GEOCODING_STREETNUMBER = "street_number";
	private static String REVERSE_GEOCODING_ROUTE = "route";
	private static String REVERSE_GEOCODING_LOCALITY = "locality";
	private static String REVERSE_GEOCODING_ADMINISTRATIVE_AREA_LEVEL_3 = "administrative_area_level_3";
//	private static String REVERSE_GEOCODING_ADMINISTRATIVE_AREA_LEVEL_2 = "administrative_area_level_2";
	private static String REVERSE_GEOCODING_ADMINISTRATIVE_AREA_LEVEL_1 = "administrative_area_level_1";
//	private static String REVERSE_GEOCODING_COUNTRY = "country";
	private static String REVERSE_GEOCODING_POSTAL_CODE = "postal_code";
	
	String TARGET_POST = "http://192.168.56.1/tugas_akhir/api/shipment/";
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shipper_register);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		geoCoding = new HashMap<String,String>();
		
		spinnerCategory = (Spinner) findViewById(R.id.spinCategory);
		
		spinnerListingDuration = (Spinner) findViewById(R.id.listing_duration_spinner);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.listing_duration, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerListingDuration.setAdapter(adapter);
		
		categoriesList = new ArrayList<Category>();
		new GetCategories().execute();
		
		checkEmail = (EditText) findViewById(R.id.register_email);
		
		checkEmail.setOnFocusChangeListener(new OnFocusChangeListener(){

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus && !checkEmail.getText().toString().equals("")){
					new CheckEmail().execute();
				}
				
			}
			
		});
		
		TextView pickupCurrentLocation = (TextView) findViewById(R.id.pick_current_position); 
		pickupCurrentLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				new FetchLocation().execute("PICKUP");
			}
		});
		
		TextView deliverCurrentLocation = (TextView) findViewById(R.id.deliver_current_position);
		deliverCurrentLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				new FetchLocation().execute("DELIVER");
			}
		});
		
		//data submit process
		TextView submitBtn = (TextView) findViewById(R.id.post_shipment_btn);
		submitBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("unchecked")
			@Override
			public void onClick(View arg0) {
				
				EditText email = (EditText) findViewById(R.id.register_email);
				final String strEmail = email.getText().toString();
				if(strEmail.length() == 0){
					email.setError("Email field required");
				}
				
				EditText password = (EditText) findViewById(R.id.register_password);
				final String strPassword = password.getText().toString();
				if(strPassword.length() == 0){
					password.setError("Password field required");
				}
				
				EditText phone = (EditText) findViewById(R.id.phone_number);
				final String strPhone = phone.getText().toString();
				if(strPhone.length() == 0){
					phone.setError("Phone field required");
				}
				
				CheckBox subscribeToNewsLetter = (CheckBox) findViewById(R.id.subscribe_to_newsletter);
				CheckBox subscribeToNotification = (CheckBox) findViewById(R.id.subscribe_to_notification);
				
				int status_newsletter = 0;
				int status_notification = 0;
				if(subscribeToNewsLetter.isChecked()){
					status_newsletter = 1;
				}
				
				if(subscribeToNotification.isChecked()){
					status_notification = 1;
				}
				
				EditText shipmentName = (EditText) findViewById(R.id.shipment_name);
				final String strShipmentName = shipmentName.getText().toString();
				if(strShipmentName.length() == 0){
					shipmentName.setError("Shipment name field required");
				}
				
				Spinner type = (Spinner) findViewById(R.id.spinCategory);
				
				EditText detail = (EditText) findViewById(R.id.shipment_detail);
				final String strDetail = detail.getText().toString();
				if(strDetail.length() == 0){
					detail.setError("Detail field required");
				}
				
				//pickup block
				EditText pickupAddr = (EditText) findViewById(R.id.shipment_pick_addr);
				final String strPickupAddr = pickupAddr.getText().toString();
				if(strPickupAddr.length() == 0){
					pickupAddr.setError("Pickup address field required");
				}
				
				EditText pickupCity = (EditText) findViewById(R.id.shipment_pick_city);
				final String strPickupCity = pickupCity.getText().toString();
				if(strPickupCity.length() == 0){
					pickupCity.setError("Pickup city required");
				}
				
				EditText pickupProvCode = (EditText) findViewById(R.id.shipment_pick_province);
				final String strPickupProvCode = pickupProvCode.getText().toString();
				if(strPickupProvCode.length() == 0){
					pickupProvCode.setError("Pickup province required");
				}
				
				EditText pickupPostalCode = (EditText) findViewById(R.id.shipment_pick_postalcode);
				final String strPickupPostalCode = pickupPostalCode.getText().toString();
				if(strPickupPostalCode.length() == 0){
					pickupPostalCode.setError("Pickup postal code required");
				}
				
				EditText deliverAddr = (EditText) findViewById(R.id.shipment_deliver_addr);
				final String strDeliverAddr = deliverAddr.getText().toString();
				if(strDeliverAddr.length() == 0){
					deliverAddr.setError("Deliver address required");
				}
				
				EditText deliverCity = (EditText) findViewById(R.id.shipment_deliver_city);
				final String strDeliverCity = deliverCity.getText().toString();
				if(strDeliverCity.length() == 0){
					deliverCity.setError("Deliver city required");
				}
				
				EditText deliverProvCode = (EditText) findViewById(R.id.shipment_deliver_province);
				final String strDeliverProvCode = deliverProvCode.getText().toString();
				if(strDeliverProvCode.length() == 0){
					deliverProvCode.setError("Deliver province required");
				}
				
				EditText deliverPostalCode = (EditText) findViewById(R.id.shipment_deliver_postalcode);
				final String strDeliverPostalCode = deliverPostalCode.getText().toString();
				if(strDeliverPostalCode.length() == 0){
					deliverPostalCode.setError("Deliver postal code required");
				}
				
				Spinner listingDuration = (Spinner) findViewById(R.id.listing_duration_spinner);
				EditText priceOffered = (EditText) findViewById(R.id.budget_offered);
				
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("email", strEmail));
				params.add(new BasicNameValuePair("password", strPassword));
				params.add(new BasicNameValuePair("phone", strPhone));
				params.add(new BasicNameValuePair("subscribe_newsletter",String.valueOf(status_newsletter)));
				params.add(new BasicNameValuePair("subscribe_push_notification",String.valueOf(status_notification)));
				params.add(new BasicNameValuePair("name",strShipmentName));
				params.add(new BasicNameValuePair("type",type.getSelectedItem().toString()));
				params.add(new BasicNameValuePair("detail",strDetail));
				
				params.add(new BasicNameValuePair("listing_duration",listingDuration.getSelectedItem().toString()));
				params.add(new BasicNameValuePair("price_offered",priceOffered.getText().toString()));
				
				params.add(new BasicNameValuePair("pickup_addr", strPickupAddr));
				params.add(new BasicNameValuePair("pickup_city", strPickupCity));
				params.add(new BasicNameValuePair("pickup_prov_code", strPickupProvCode));
				params.add(new BasicNameValuePair("pickup_postal_code", strPickupPostalCode));
				
				params.add(new BasicNameValuePair("deliver_addr", strDeliverAddr));
				params.add(new BasicNameValuePair("deliver_city", strDeliverCity));
				params.add(new BasicNameValuePair("deliver_prov_code", strDeliverProvCode));
				params.add(new BasicNameValuePair("deliver_postal_code", strDeliverPostalCode));
				//validation OK
				new PostShipment().execute(params);
			}
		});
	}
	
	private class PostShipment extends AsyncTask<List<NameValuePair>,Void,Void>{
		
		@Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        pDialog = new ProgressDialog(PostShipmentActivity.this);
	        pDialog.setMessage("Processing..");
	        pDialog.setCancelable(false);
	        pDialog.show();
	    }
		
		@Override
		protected Void doInBackground(final List<NameValuePair>... arg0) {
			final List<NameValuePair> params = arg0[0];
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
			PostShipmentActivity.this.runOnUiThread(new Runnable(){

				@Override
				public void run() {
					HTTPRequest httpRequest = new HTTPRequest(TARGET_POST);
					String response = httpRequest.makeRequest(HTTPRequest.POST, params);
					try{
						JSONObject jObjResponse = new JSONObject(response);
						String status = jObjResponse.getString("success");
						String shipment_id = jObjResponse.getString("shipmentid");
						if(status.equals("success")){
							
							Intent addPic = new Intent(getApplicationContext(), PostShipmentPictureActivity.class);
							
							Bundle bundle = new Bundle();
							bundle.putString("SHIPMENT_DETAIL_ID", shipment_id);
							addPic.putExtras(bundle);
							
							addPic.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					        startActivity(addPic);
					        finish();
						}
					}catch(JSONException e){
						Toast.makeText(PostShipmentActivity.this, response, Toast.LENGTH_LONG).show();
					}
					
				}
				
			});
			
			return null;
		}
		
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
	        if (pDialog.isShowing())
	            pDialog.dismiss();
	    }
		
	}
	
	private class FetchLocation extends AsyncTask<String,Void,Void>{
		
		@Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        pDialog = new ProgressDialog(PostShipmentActivity.this);
	        pDialog.setMessage("Fetching location..");
	        pDialog.setCancelable(false);
	        pDialog.show();
	    }
		
		@Override
		protected Void doInBackground(final String... unused) {
			
			PostShipmentActivity.this.runOnUiThread(new Runnable(){
				int shipment_addr = 0, shipment_city=0,shipment_province=0,shipment_postalcode=0;
				
				@Override
				public void run() {
					String param = unused[0];
					if(param.equals("PICKUP")){
						shipment_addr = R.id.shipment_pick_addr;
						shipment_city = R.id.shipment_pick_city;
						shipment_province = R.id.shipment_pick_province;
						shipment_postalcode = R.id.shipment_pick_postalcode;
					} else if(param.equals("DELIVER")){
						shipment_addr = R.id.shipment_deliver_addr;
						shipment_city = R.id.shipment_deliver_city;
						shipment_province = R.id.shipment_deliver_province;
						shipment_postalcode = R.id.shipment_deliver_postalcode;
					}
					
					double longitude=0,latitude=0;
					
					GPSTracker gpsTracker = new GPSTracker(getApplicationContext());
					if(gpsTracker.canGetLocation()){
						latitude = gpsTracker.getLatitude();
						longitude = gpsTracker.getLongitude();
						
					}else{
						gpsTracker.showSettingsAlert();
					}
					
					if(latitude == 0 || longitude == 0){
						latitude = 61.48354999999999;
						longitude = -149.72036;
					}
					StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
					StrictMode.setThreadPolicy(policy);
					HTTPRequest httpRequest = new HTTPRequest("http://192.168.56.1/tugas_akhir/api/shipment/geocoding/"+latitude+"/"+longitude);
					
					String response = httpRequest.makeRequest(HTTPRequest.GET, null);
					try {
						JSONObject jObjResponse = new JSONObject(response);
						String status = jObjResponse.getString("results");
						if(status.equals("OK")){
							
							String streetNumber = "";
							String route = "";
							String admLevel3 = "";
							//street_number
							if(jObjResponse.getString(REVERSE_GEOCODING_STREETNUMBER) != null){
								streetNumber = jObjResponse.getString(REVERSE_GEOCODING_STREETNUMBER);
							}
							
							if(jObjResponse.getString(REVERSE_GEOCODING_ROUTE) != null){
								route = jObjResponse.getString(REVERSE_GEOCODING_ROUTE);
							}
							
							if(jObjResponse.getString(REVERSE_GEOCODING_ADMINISTRATIVE_AREA_LEVEL_3) != null){
								admLevel3 = jObjResponse.getString(REVERSE_GEOCODING_ADMINISTRATIVE_AREA_LEVEL_3);
							}
							
							EditText addr = (EditText) findViewById(shipment_addr);
							addr.setText(streetNumber+" "+route+", "+admLevel3);
							
							if(jObjResponse.getString(REVERSE_GEOCODING_LOCALITY) != null){
								EditText city = (EditText) findViewById(shipment_city);
								city.setText(jObjResponse.getString(REVERSE_GEOCODING_LOCALITY));
							}
							
							if(jObjResponse.getString(REVERSE_GEOCODING_ADMINISTRATIVE_AREA_LEVEL_1) != null){
								EditText stateCode = (EditText) findViewById(shipment_province);
								stateCode.setText(jObjResponse.getString(REVERSE_GEOCODING_ADMINISTRATIVE_AREA_LEVEL_1));
							}
							
							if(jObjResponse.getString(REVERSE_GEOCODING_POSTAL_CODE)!= null){
								EditText postalCode = (EditText) findViewById(shipment_postalcode);
								postalCode.setText(jObjResponse.getString(REVERSE_GEOCODING_POSTAL_CODE));
							}
							
						} else if(status.equals("ZERO_RESULTS")){
							Toast.makeText(PostShipmentActivity.this, "Unable to get location", Toast.LENGTH_LONG).show();
						}
						
						
					} catch (JSONException e) {
						e.printStackTrace();
						Toast.makeText(PostShipmentActivity.this, "JSON Exception", Toast.LENGTH_LONG).show();
					}
				}
				
			});
			
			
			return null;
		}
		
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
	        if (pDialog.isShowing())
	            pDialog.dismiss();
	    }
		
	}
	/**
	 * ASync : check email registered 
	 * */
	private class CheckEmail extends AsyncTask<Void, Void, Void>{
		
		@Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        pDialog = new ProgressDialog(PostShipmentActivity.this);
	        pDialog.setMessage("Fetching categories..");
	        pDialog.setCancelable(false);
	        pDialog.show();
	    }
		
		@Override
		protected Void doInBackground(Void... arg) {
			PostShipmentActivity.this.runOnUiThread(new Runnable(){
				
				@Override
				public void run() {
					String email = checkEmail.getText().toString();
					try {
						
						String checkEmailURL = "http://192.168.56.1/tugas_akhir/api/user/checkemail/" 
							+ URLEncoder.encode(email, "ISO-8859-1");
						
						StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
						StrictMode.setThreadPolicy(policy);
						
						HTTPRequest httpRequest = new HTTPRequest(checkEmailURL);
						String response = httpRequest.makeRequest(HTTPRequest.GET, null);
						
						JSONObject jObjResponse = new JSONObject(response);
						if(jObjResponse.getString("response").equals("false")){
							checkEmail.setText("");
							Toast.makeText(PostShipmentActivity.this, "Email "+email+" already registered. Please use another email or login", Toast.LENGTH_LONG).show();
						} else if(jObjResponse.getString("response").equals("true")){
							Toast.makeText(PostShipmentActivity.this, "OK. "+email+" ready to use.", Toast.LENGTH_LONG).show();
						}
						
						
					} catch (UnsupportedEncodingException e) {
						Toast.makeText(PostShipmentActivity.this, "Unsupport encoding error", Toast.LENGTH_LONG).show();
					} catch (JSONException e) {
						Toast.makeText(PostShipmentActivity.this, "JSON Exception", Toast.LENGTH_LONG).show();
					} 
				}
				
			});
			
			return null;
		}
		
		protected void onPostExecute(Void result) {
	        super.onPostExecute(result);
	        if (pDialog.isShowing())
	            pDialog.dismiss();
	    }
		
	}
	
	/**
	 * ASync : get list of category fetched from database
	 * */
	private class GetCategories extends AsyncTask<Void, Void, Void>{
		
		@Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        pDialog = new ProgressDialog(PostShipmentActivity.this);
	        pDialog.setMessage("Fetching categories..");
	        pDialog.setCancelable(false);
	        pDialog.show();
	 
	    }
		
		@Override
		protected Void doInBackground(Void... arg0) {
			String getCategoryURL = "http://192.168.56.1/tugas_akhir/api/shipment/category/";
			JSONArray jArray = null;
			
			HTTPRequest httpRequest = new HTTPRequest(getCategoryURL);
			String response = httpRequest.makeRequest(HTTPRequest.GET, null);
			try {
	            jArray = new JSONArray(response);
	        } catch (JSONException e) {
	        	Toast.makeText(getApplicationContext(), "JSON Exception", Toast.LENGTH_LONG).show();
	        }
			
			for (int i = 0; i < jArray.length(); i++) {
				try{
					JSONObject row = jArray.getJSONObject(i);
					Category cat = new Category(
							Integer.parseInt(row.getString("type")),
							row.getString("shipment_type_name"));
					categoriesList.add(cat);
					
				} catch (JSONException je){
					Toast.makeText(getApplicationContext(), "JSON Exception", Toast.LENGTH_LONG).show();
				}
			}
			
			return null;
		}
		
		@Override
	    protected void onPostExecute(Void result) {
	        super.onPostExecute(result);
	        if (pDialog.isShowing())
	            pDialog.dismiss();
	        populateSpinner();
	    }
		
	}
	
	private void populateSpinner(){
		List<String> lables = new ArrayList<String>();
		
		for (int i = 0; i < categoriesList.size(); i++) {
	        lables.add(categoriesList.get(i).getName());
	    }
		
		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, 
				android.R.layout.simple_spinner_item, lables);
		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerCategory.setAdapter(spinnerAdapter);
		
	}
	
	
}
